# terraform main script
terraform {
  backend "gcs" {
     bucket = "terraformbucket"
     prefix = "awsome/resources"
     credentials = "tf-state-key.json"
     project = "terraform-228016"
     region =  "us-central1"
    }
}
# configure the Google Cloud provider
 provider "google" {
   version =  "1.18.0"
   project =  "${var.project_id}"
   region =  "${var.region}"
   zone = "${var.zone}"
   credentials =  "${var.terraform_admin_key}"
}

