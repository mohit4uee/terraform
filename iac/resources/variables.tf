variable "project_id" {
  description = "The unique id for the project"
  default ="terraform-228016"
}
variable "project_name" {
  description = "The human readable name for the project"
  default = "terraform"
}

variable "terraform_admin_key" {
  description = "The SA key for the Terraform Admin account for the target project"
}

variable "region" {
  description = "The default region for regional project resources"
  default     = "us-central1"
}
variable "zone" {
  description = "The default zone for zonal project resources"
  default     = "us-central1-b"
}

variable "shared_vpc_project_id" {
  description = "The id of the shared vpc host project"
  default     = "terraform-228016"
}

variable no_external_ip_access_config {
  description = "The access config block on instance for which we want no External IP"
  type        = "list"

  // set to [] to remove external IP
  default = []
}
variable "compute_sa_name" {
  description = "The name of the Terraform admin account to be created in the project"
  default     = "project-compute-user"
}

// General Variables
variable "linux_admin_username" {
    type        = "string"
    description = "User name for authentication to the Kubernetes linux agent virtual machines in the cluster."
    default = "mohit"
}
variable "linux_admin_password" {
    type ="string"
    description = "The password for the Linux admin account."
    default = "password12345678"
}
// GCP Variables
variable "gcp_cluster_count" {
    type = "string"
    description = "Count of cluster instances to start."
    default = "1"
}
variable "cluster_name" {
    type = "string"
    description = "Cluster name for the GCP Cluster."
    default  = "krb-cluster"
}
