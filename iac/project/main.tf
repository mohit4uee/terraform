provider "google" {
   version =  "1.18.0"
   project =  "${var.project_name}"
   region =  "${var.region}"
   zone = "${var.zone}"
   credentials =  "${var.terraform_admin_key}"
}
