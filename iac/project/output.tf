# generated id for new project
output "project_id" {
  value = "${google_project.project.project_id}"
}

# generated number for new project
output "project_number" {
  value = "${google_project.project.number}"
}
