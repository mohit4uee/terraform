variable "project_name" {
  description = "The human readable name for the project"
}




variable "billing_account" {
  description = "The billing account id for the project"
}

variable "region" {
  description = "The default region for regional project resources"
  default     = "us-central1"
}
variable "zone" {
  description = "The default zone for zonal project resources"
  default     = "us-central1-b"
}


variable "project_apis" {
  description = "APIs to be enabled on the newly created project"
  type        = "list"

  default = [
    "iam.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "storage-api.googleapis.com",
    "oslogin.googleapis.com",
    "bigquery-json.googleapis.com",
    "containerregistry.googleapis.com",
    "pubsub.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "dataproc.googleapis.com",
    "storage-component.googleapis.com",
    "deploymentmanager.googleapis.com",
    "replicapool.googleapis.com",
    "replicapoolupdater.googleapis.com",
    "resourceviews.googleapis.com",
    "iamcredentials.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "stackdriver.googleapis.com",
  ]
}

variable "terraform_admin_key" {
  description = "The SA key for the Terraform Admin account for the target project"
}



